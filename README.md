# OpenML dataset: delta_elevators

https://www.openml.org/d/571

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This data set is also obtained from the task of controlling the
ailerons of a F16 aircraft, although the target variable and
attributes are different from the ailerons domain. The target variable
here is a variation instead of an absolute value, and there was some
pre-selection of the attributes.

Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
Original source: Experiments of Rui Camacho (rcamacho@garfield.fe.up.pt).
Characteristics: 7129 cases, 7 continuous attributes

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/571) of an [OpenML dataset](https://www.openml.org/d/571). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/571/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/571/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/571/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

